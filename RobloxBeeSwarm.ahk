﻿#SingleInstance force
CoordMode, Mouse, Screen
SetKeyDelay -1
SetMouseDelay 0
SetBatchLines -1

isClicking:=False

StartHotkey := "F6"
prevStart := StartHotkey
Hotkey, ~%StartHotkey%, StartClicking

Gui, a: New, hwndhGui AlwaysOnTop Resize MinSize

Gui, Add, Text,section, Bee Swarm Macro by Nom

Gui, Add, CheckBox, vHoldLeftClick Checked, Hold left click

Gui, Add, Text,ys+45 r1 xs, Pattern:
Gui, Add, DropDownList,w80 yp-3 x+22 vRunPattern, Square|Snake||None

Gui, Add, Button, xs w180 h30 gStartClicking, Start/Stop
Gui, Add, Text,xs y+5, Set hotkey: 
Gui, Add, Hotkey, yp-2 xp+60 w60 gSetHotkeys vStartHotkey, %StartHotkey%

Gui, Add, Text,xs vStatus w180, Idle

Gui, Add, Link,xs, <a href="https://www.patreon.com/nomscripts">Patreon</a> Support me and Learn
Gui, Add, Link,xs, AHK along the way!

Gui, Show,, Bee Swarm Macro by Nom
OnMessage(0x112, "WM_SYSCOMMAND")

return

SetHotkeys:
if (StartHotkey) {
	Hotkey, ~%prevStart%, StartClicking, OFF
	Hotkey, ~%StartHotkey%, StartClicking, ON
	prevStart := StartHotkey
}
UpdateText("Status", "Start: " StartHotkey)
return

ClickTimer:
if (HoldLeftClick == 1) {
	MouseClick , , ,, 1, , D
}
if (RunPattern == "Square") {
	square()
}
if (RunPattern == "Snake") {
	snake()
}
return

square() {
	global isClicking
	sleepTime := 1000
	loop {
		if (!isClicking) {
			return
			if (HoldLeftClick == 1) {
				MouseClick , , ,, 1, , U
			}
		}
		
		Send {w down}
		Sleep, sleepTime
		Send {w up}
		
		if (!isClicking) {
			return
		}
		Send {d down}
		Sleep, sleepTime
		Send {d up}
		
		if (!isClicking) {
			return
		}
		Send {s down}
		Sleep, sleepTime
		Send {s up}
		
		if (!isClicking) {
			return
		}
		Send {a down}
		Sleep, sleepTime
		Send {a up}
	}
}

snake() {
	global isClicking
	turn := 300
	longWalk := 1500
	loop {
		if (!isClicking) {
			return
			if (HoldLeftClick == 1) {
				MouseClick , , ,, 1, , U
			}
		}
		
		Send {w down}
		Sleep, longWalk
		Send {w up}
		
		loop, 2 {
		
			Send {d down}
			Sleep, turn
			Send {d up}
			
			if (!isClicking) {
				return
			}
			Send {s down}
			Sleep, longWalk
			Send {s up}
		
			if (!isClicking) {
				return
			}
		
			Send {d down}
			Sleep, turn
			Send {d up}
			
			if (!isClicking) {
				return
			}
			Send {w down}
			Sleep, longWalk
			Send {w up}
		
			if (!isClicking) {
				return
			}
		}
		
		
		Send {d down}
		Sleep, turn
		Send {d up}
		
		if (!isClicking) {
			return
		}
		Send {s down}
		Sleep, longWalk
		Send {s up}
	
		if (!isClicking) {
			return
		}
		
		Send {a down}
		Sleep, longWalk
		Send {a up}
	}
}

StartClicking:
Gui, a: Submit, Nohide
if (!isClicking) {
	isClicking:=True
	Settimer, ClickTimer, -1
	UpdateText("Status", "Running!")
} else {
	isClicking:=False
	UpdateText("Status", "Idle")
}
return

UpdateText(ControlID, NewText)
{
	static OldText := {}
	global hGui
	if (OldText[ControlID] != NewText)
	{
		GuiControl, %hGui%:, % ControlID, % NewText
		OldText[ControlID] := NewText
	}
}


WM_SYSCOMMAND(wp, lp, msg, hwnd)  {
   static SC_CLOSE := 0xF060
   if (wp != SC_CLOSE)
      Return
   
   ExitApp
}