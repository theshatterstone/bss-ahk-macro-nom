# A Bee Swarm Simulator Macro for Linux using AHK

Tutorial here: https://www.reddit.com/r/BeeSwarmSimulator/comments/10v1zny/a_guide_to_macroing_on_linux/ 

This macro works on Linux, if set up properly.

This macro wasn't made by me, it was made by Nom.
Full credit goes to Nom for creating this macro.

Requires AutoHotkey set up properly, according to the tutorial linked above.
